package com.example.demo.sql;

import java.util.ArrayList;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


@Mapper
public interface DemoSqlMapper {

	@Select("SELECT * FROM company")
	ArrayList<Map<String, Object>> test();
}
