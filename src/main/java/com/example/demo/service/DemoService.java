package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.sql.DemoSqlMapper;


@Service
public class DemoService {

	@Autowired
	DemoSqlMapper sql;

	public String test() {
		return sql.test().toString();
	}
}
